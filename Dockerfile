FROM python:3.8-slim

WORKDIR /home/bot
COPY requirements.txt ./

RUN pip3 install -r ./requirements.txt

COPY . .

ENV PYTHONPATH = ${PYTHONPATH}:/home/bot/app

ENTRYPOINT ["python3", "app/main.py"]
