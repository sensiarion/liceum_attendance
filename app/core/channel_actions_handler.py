from typing import List, Callable, Coroutine

import discord

VoiceHandlerSignature = Callable[[discord.Member, discord.VoiceState, discord.VoiceState], Coroutine]


class VoiceActionHandlerCollection:
    """
    Обёртка для добавления обработчиков на события связанные с чатом
    """

    # TODO переписать на обвязку с вызовом всех внутренних функций, дабы не нарушать изоляцию

    def __init__(self):
        self._check_in_handlers: List[VoiceHandlerSignature] = []
        self._check_out_handlers: List[VoiceHandlerSignature] = []

    def check_in(self):
        """
        **Декоратор** для регистрации на **подключение** к чату
        """

        # TODO check input function typing to ensure correct func signature
        def wrapper(func: VoiceHandlerSignature):
            self._check_in_handlers.append(func)
            return func

        return wrapper

    def check_out(self):
        """
        **Декоратор** для регистрации на **отключение** от чату
        """

        # TODO check input function typing to ensure correct func signature
        def wrapper(func: VoiceHandlerSignature):
            self._check_out_handlers.append(func)
            return func

        return wrapper

    def register_voice(self, command_block: 'VoiceActionHandlerCollection'):
        self._check_in_handlers.extend(command_block._check_in_handlers)
        self._check_out_handlers.extend(command_block._check_out_handlers)
