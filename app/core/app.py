import logging

import discord

from core.channel_actions_handler import VoiceActionHandlerCollection
from core.message_handler import CommandHandler


class DiscordBot(discord.Client, CommandHandler, VoiceActionHandlerCollection):
    def __init__(self, **options):
        discord.Client.__init__(self, **options)
        CommandHandler.__init__(self)
        VoiceActionHandlerCollection.__init__(self)

        self.logger = logging.getLogger('discord-bot')

    async def on_ready(self):
        self.logger.info('Logged on as {0}!'.format(self.user))

    async def on_message(self, message: discord.Message):
        self.logger.debug('Message from {0.author}: {0.content}'.format(message))
        await CommandHandler.call(self, message)

    async def on_voice_state_update(self, member: discord.Member, before: discord.VoiceState,
                                    after: discord.VoiceState):
        self.logger.debug('changing voce chat state',
                          extra={'member': member, 'before': before, 'after': after})

        if before.channel is None and after.channel is not None:
            for handler in self._check_in_handlers:
                await handler(member, before, after)
        elif before.channel is not None and after.channel is None:
            for handler in self._check_out_handlers:
                await handler(member, before, after)
        else:
            self.logger.warning('voice state update unexpected action')
