import abc
import enum
from typing import Callable, List, Tuple, Optional, NamedTuple, Coroutine

import discord


class HandlerMatchType(enum.Enum):
    command = 'command'
    regexp = 'regexp'


class Command(NamedTuple):
    command: str
    data: str


# Описания типов объектов, для помощи IDE
HandlerSignature = Callable[[Command, discord.Member, discord.Message], Coroutine]
HandlerPattern = str


class CommandCollection:
    """
    Класс, отвечающий за сбор обработчиков входящих сообщений
    """

    def __init__(self):
        self._bot_handlers: List[Tuple[HandlerMatchType, HandlerPattern, HandlerSignature]] = []
        # noinspection PyTypeChecker
        self._client: discord.Client = None

    def _add_handler(self, func: HandlerSignature, match_type: HandlerMatchType, pattern: HandlerPattern):
        if match_type == HandlerMatchType.command:
            self._bot_handlers.append((HandlerMatchType.command.value, pattern, func))
        else:
            raise NotImplementedError()

    def command(self, command: str):
        """
        **Декоратор** для указания обработчика команды.

        :param command: команда, на которую должен реагировать обработчик
        """

        def wrapper(func: HandlerSignature):
            self._add_handler(func, HandlerMatchType.command, command)
            return func

        return wrapper

    @property
    def client(self) -> discord.Client:
        """
        Получение текущего инстанса клиента для взаимодействия.

        Получение возможно только после прикрепления набора хэндлеров
        к основному приложению с помощью метода "register"
        """
        if not self._client:
            raise AttributeError('You should bind handlers to client by calling "register" method first')
        return self._client

    def register(self, command_block: 'CommandCollection'):
        """
        Добавления блока хэндлеров к приложению

        :param command_block: набор обработчиков с командами
        """
        if not isinstance(self, (discord.Client, self.__class__)):
            raise TypeError("Unable to register block command in another command block")
        # noinspection PyUnresolvedReferences
        self._bot_handlers.extend(command_block._bot_handlers)

        # немного кривая, но всё таки поздняя инициализация,
        # гарантирующая наличие клиента у каждого зарегистрированного хэндлера
        command_block._client = self


class CommandHandler(CommandCollection):

    def __init__(self, command_prefix: str = '!'):
        super().__init__()
        if not command_prefix:
            raise ValueError('Command prefix should be filled')
        if any([i.isspace() for i in command_prefix]):
            raise ValueError('Space symbols are not allowed in command prefix')

        self.command_prefix = command_prefix

    def is_command(self, content: str) -> bool:
        """
        Является ли сообщение командой

        :param content: содержимое сообщения
        """
        return content.startswith(self.command_prefix)

    def extract_command(self, content: str) -> Command:
        """
        Извлеченные данных по вызванной команде
        :param content: содержимое сообщения
        :return: извлечённая команда и данные по команде
        """
        raw_command, *rest = content.split()
        data = content.replace(raw_command, '', 1).lstrip()
        command = raw_command.replace(self.command_prefix, '', 1)

        return Command(command, data)

    def match_handler(self, message: discord.Message) -> Optional[Tuple[HandlerSignature, Command]]:
        """
        Поиск подходящего обработчика среди зарегистрированных
        :param message: сообщение пользователя
        :return: кортеж из функции обработчика и извлечённой сущности
        """
        content: str = message.content
        for match_type, pattern, handler in self._bot_handlers:
            if self.is_command(content) and match_type == HandlerMatchType.command.value:
                parsed_command = self.extract_command(content)
                if parsed_command.command == pattern:
                    return handler, parsed_command

    async def call(self, message: discord.Message):
        if match := self.match_handler(message):
            handler, command = match
            return await handler(command, message.author, message)
