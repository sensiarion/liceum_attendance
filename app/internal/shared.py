import discord

from core.message_handler import HandlerSignature


def is_admin(user: discord.Member) -> bool:
    return getattr(user.guild_permissions, 'administrator', False)


def ensure_admin(func: HandlerSignature):
    """
    Декоратор для проверки наличия прав администратора
    """

    async def handler_wrapper(command, member: discord.Member, message: discord.Message, *args, **kwargs):
        if not is_admin(member):
            await message.reply('Данная команда требует наличия прав администратора')
            return
        return await func(command, member, message)

    return handler_wrapper
