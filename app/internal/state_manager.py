import contextvars
import uuid
from typing import Dict, Optional

import discord

from internal.lesson_session import LessonSession


class StateManager:
    """
    **Статический** объект для управления состоянием всего приложения на контекстных переменных
    """
    __lessons = dict()

    @classmethod
    def lessons(cls) -> Dict[uuid.UUID, LessonSession]:
        return cls.__lessons

    @classmethod
    def get_lesson(cls, channel: discord.VoiceChannel) -> Optional[LessonSession]:
        return cls.__lessons.get(channel.id)

    @classmethod
    def pop_lesson(cls, channel: discord.VoiceChannel) -> LessonSession:
        """
        :raises: KeyError: при отсутствии занятия в указанном канале
        """
        return cls.__lessons.pop(channel.id)

    @classmethod
    def add_lesson(cls, lesson: LessonSession) -> bool:
        if lesson.channel.id in cls.__lessons:
            return False

        cls.__lessons[lesson.channel.id] = lesson
        return True
