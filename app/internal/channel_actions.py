import logging

import discord

from core.channel_actions_handler import VoiceActionHandlerCollection
from internal.state_manager import StateManager

attendance_handlers = VoiceActionHandlerCollection()
logger = logging.getLogger('attendance-handlers')


@attendance_handlers.check_in()
async def on_chat_enter(member: discord.Member, before: discord.VoiceState, after: discord.VoiceState):
    channel: discord.VoiceChannel = after.channel
    lesson = StateManager.get_lesson(channel)
    if not lesson:
        logger.debug(f'No lesson found in channel {channel.name}; {channel.id=}')
        return

    await lesson.participant_enter(member)


@attendance_handlers.check_out()
async def on_chat_exit(member: discord.Member, before: discord.VoiceState, after: discord.VoiceState):
    channel: discord.VoiceChannel = before.channel
    lesson = StateManager.get_lesson(channel)
    if not lesson:
        logger.debug(f'No lesson found in channel {channel.name}; {channel.id=}')
        return

    await lesson.participant_out(member)
