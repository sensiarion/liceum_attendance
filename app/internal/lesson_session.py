import asyncio
import datetime
import enum
import math
import uuid
from collections import OrderedDict
from typing import Dict, Any, Optional, Literal, Collection, List, TypeVar, Union

import discord

from internal.shared import is_admin


class ActionTypes(enum.Enum):
    log_in = 'log_in'
    log_out = 'log_out'


class Log:
    def __init__(self, action: ActionTypes, created_at: datetime.datetime = None):
        """
        Объект для получения информации о входе/выходе в голосовой канал

        :param created_at: время входа выхода
        :param action: действие, которое совершил пользователь
        """
        self.action = action
        self.created_at = created_at if created_at is not None else datetime.datetime.utcnow()


class Participant:
    # немного полезной информации nick – имя в конкретном чате, а name – общее имя
    # чтобы получить правильную роль, есть прекрасное свойство member.top_role
    def __init__(self, user: discord.Member):
        self.user = user
        self._history: List[Log] = []

    @property
    def history(self):
        return self._history.copy()  # read only

    async def check_in(self):
        # TODO call lock for db fetching (ensure order)
        self._history.append(Log(ActionTypes.log_in))

    async def check_out(self):
        # if self._history and self._history[-1].action != ActionTypes.log_in:
        #     await self.check_in()
        self._history.append(Log(ActionTypes.log_out))

    def to_dict(self) -> dict:
        return {
            'id': self.user.id,
            'nick': self.user.nick,
            'common_name': self.user.name,
            'role': {
                'id': self.user.top_role.id,
                'name': self.user.top_role.name,
                'color': str(self.user.top_role.color)
            },
            'is_admin': is_admin(self.user),
            'history': [{'created_at': i.created_at, 'action': i.action} for i in self.history]

        }

    def total_online(self) -> int:
        """
        Время, проведённое на уроке в минутах
        :raises ValueError: if log sequence is incorrect
        """
        total = datetime.timedelta()
        if not self.history:
            return math.ceil(total.total_seconds() / 60)

        history = self.history.copy()
        if history[-1].action.value != ActionTypes.log_out.value:
            history.append(Log(ActionTypes.log_out))

        # noinspection PyTypeChecker
        for i in range(0, len(history), 2):
            check_in, check_out = history[i], history[i + 1]
            if check_in is None:
                continue

            if check_in.action.value != ActionTypes.log_in.value:
                raise ValueError(
                    f"start action is \"{check_in.action.value}'\"; expected \"{ActionTypes.log_in.value}\""
                )
            if check_out.action.value != ActionTypes.log_out.value:
                raise ValueError(
                    f"end action is \"{check_out.action.value}'\"; expected \"{ActionTypes.log_out.value}\""
                )
            check_in_moment = check_in.created_at
            check_out_moment = check_out.created_at if check_out else datetime.datetime.utcnow()
            total += check_out_moment - check_in_moment

        return math.ceil(total.total_seconds() / 60)


User = discord.Member


class LessonSession:
    # TODO restore on startup
    # sync with db
    # log all income, outcome to chat

    # TODO store first statistic message id

    def __init__(
            self,
            channel: discord.VoiceChannel,
            text_channel: discord.TextChannel,
            started_at: datetime.datetime = None
    ):
        """
        :param channel: канал, в котором будет проходить урок
        :param text_channel: канал, в котором была отдана команда на запуск урока
        """
        self.text_channel = text_channel
        self.channel = channel
        self.participants: Dict[int, Participant] = OrderedDict()
        self._id = uuid.uuid4()
        self.started_at = datetime.datetime.utcnow() if started_at is None else started_at

    @property
    def id(self) -> uuid.UUID:
        return self._id

    def _get_participant(self, user: User) -> Optional[Participant]:
        return self.participants.get(user.id)

    async def _add_participant(self, user: User) -> Participant:
        participant = Participant(user)
        self.participants[user.id] = participant
        return participant

    async def participant_enter(self, user: User):
        if not self._get_participant(user):
            participant = await self._add_participant(user)
            await participant.check_in()
        else:
            participant = self._get_participant(user)
            await participant.check_in()

    async def participant_out(self, user: User):
        if participant := self._get_participant(user):
            await participant.check_out()
        else:
            await self.participant_enter(user)
            await participant.check_out()

    async def load_channel_participants(self, client: discord.Client):
        """
        Подгружаем всех участников, находящихся в комнате

        :param client:
        :return:
        """
        guild: discord.Guild = await client.fetch_guild(self.channel.guild.id)
        # к сожалению, members не содержит всех участников голосового канала
        # но у нас есть идентификаторы пользователей, сидящих в канале
        states: Dict[int, discord.VoiceState] = self.channel.voice_states
        # TODO rewrite with gather method or with fetching all members
        members = [await guild.fetch_member(user_id) for user_id in states]

        for user in members:
            await self.participant_enter(user)

    async def load(self, session_id: int):
        raise NotImplementedError("Zavezi bazu")

    @staticmethod
    def attendee_repr(since_start: int, online_minutes: int):
        attendee_sign = '+'
        signs_count = 10
        percentage = (online_minutes / since_start)
        return (attendee_sign * int(percentage * signs_count)).ljust(signs_count, '-')

    async def show_statistic(self):
        seconds_spent = (datetime.datetime.utcnow() - self.started_at).total_seconds()
        minutes_spent = math.ceil(seconds_spent / 60)

        stat = '\n'.join(
            [
                f'{i.user.nick or i.user.display_name} `{self.attendee_repr(minutes_spent, i.total_online())}`'
                for i in
                sorted(self.participants.values(), key=lambda val: val.user.nick or val.user.display_name or '')
            ]
        )

        message = f'Урок идёт уже {minutes_spent} минут' \
                  f'\n{stat}'

        await self.text_channel.send(message)


async def create_lesson_session(
        client: discord.Client,
        channel: discord.VoiceChannel,
        text_channel: discord.TextChannel
) -> LessonSession:
    """
    Метод для создания сессии урока.

    Является рекомендуемым вариантом создания объекта
    :param client:
    :param channel:
    :param text_channel:
    :return:
    """
    lesson_session = LessonSession(channel, text_channel)
    await lesson_session.load_channel_participants(client)
    return lesson_session
