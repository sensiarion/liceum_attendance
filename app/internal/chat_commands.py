from typing import List

import discord

from core.message_handler import CommandCollection
from internal.lesson_session import LessonSession, create_lesson_session
from internal.shared import ensure_admin
from internal.state_manager import StateManager

default_commands = CommandCollection()


# TODO ensure admin
@default_commands.command('start')
@ensure_admin
async def session_start(command, member: discord.Member, message: discord.Message):
    if not member.voice:
        await message.reply(
            'Для запуска урока необходимо зайти в голосовой чат, в котором он будет проходить'
        )
        return
    if StateManager.get_lesson(member.voice.channel):
        await message.reply(f'В канале {member.voice.channel.name} уже проводится урок')
        return

    lesson_session = await create_lesson_session(
        default_commands.client,
        member.voice.channel,
        message.channel
    )
    StateManager.add_lesson(lesson_session)
    await message.channel.send('Урок начат')


@default_commands.command('statistic')
@ensure_admin
async def statistic(command, member: discord.Member, message: discord.Message):
    guid_channels: List[discord.TextChannel] = message.guild.text_channels

    # отправляем уведомления только по урокам из текущей гильдии
    for channel in guid_channels:
        for lesson in StateManager.lessons().values():
            if lesson.text_channel.id == channel.id:
                await lesson.show_statistic()


@default_commands.command('end')
@ensure_admin
async def session_end(command, member, message):
    if not member.voice:
        await message.reply('Я не экстрасенс, будьте добры зайти в канал, в котором проводится занятие')
        return
    lesson = StateManager.get_lesson(member.voice.channel)
    if lesson is None:
        await message.reply('В данном канале урок не начинался')
        return

    StateManager.pop_lesson(member.voice.channel)
    await message.channel.send('Расчёт окончен')
    await lesson.show_statistic()
