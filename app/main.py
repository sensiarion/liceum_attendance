import logging

from config import Config
from core.app import DiscordBot
from internal.channel_actions import attendance_handlers
from internal.chat_commands import default_commands

client = DiscordBot()

client.register(default_commands)
client.register_voice(attendance_handlers)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    client.run(Config.token)
