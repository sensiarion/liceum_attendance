from core.channel_actions_handler import VoiceActionHandlerCollection


class TestVoiceActionHandler:
    def test_check_in(self):
        coll = VoiceActionHandlerCollection()

        @coll.check_in()
        async def handle_test(member, before, after):
            pass

        assert handle_test is coll._check_in_handlers[0]

    def test_check_in_check_out_not_mixed(self):
        coll = VoiceActionHandlerCollection()

        @coll.check_in()
        async def handle_test(member, before, after):
            pass

        assert len(coll._check_out_handlers) == 0

        coll = VoiceActionHandlerCollection()

        @coll.check_out()
        async def handle_test(member, before, after):
            pass

        assert len(coll._check_in_handlers) == 0
