import datetime
from unittest.mock import patch

import pytest

from internal.lesson_session import Participant, LessonSession


class TestParticipant:
    @patch('internal.lesson_session.datetime')
    @pytest.mark.asyncio
    async def test_total_online(self, datetime_mock):
        counter = 0
        start_moment = datetime.datetime(year=2021, month=10, day=10)

        def datetime_call():
            nonlocal counter
            counter += 1
            return start_moment + datetime.timedelta(hours=counter)

        datetime_mock.datetime.utcnow = datetime_call
        datetime_mock.timedelta = datetime.timedelta

        p = Participant(None)

        await p.check_in()
        await p.check_out()
        await p.check_in()
        await p.check_out()

        results = p.total_online()
        expected_minutes = 120
        assert results == expected_minutes

    @pytest.mark.asyncio
    async def test_total_online_empty(self):
        p = Participant(None)

        results = p.total_online()
        expected_minutes = 0
        assert results == expected_minutes

    @patch('internal.lesson_session.datetime')
    @pytest.mark.asyncio
    async def test_total_online_incorrect_order(self, datetime_mock):
        with pytest.raises(ValueError):
            counter = 0
            start_moment = datetime.datetime(year=2021, month=10, day=10)

            def datetime_call():
                nonlocal counter
                counter += 1
                return start_moment + datetime.timedelta(hours=counter)

            datetime_mock.datetime.utcnow = datetime_call
            datetime_mock.timedelta = datetime.timedelta

            p = Participant(None)

            await p.check_in()
            await p.check_out()
            await p.check_in()
            await p.check_in()
            await p.check_out()

            results = p.total_online()
            expected_minutes = 120
            assert results == expected_minutes


class TestAttendeeRepr:
    def test_smallest_timing_repr(self):
        expected = '+' * 10

        result = LessonSession.attendee_repr(1, 1)

        assert result == expected
