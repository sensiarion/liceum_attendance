from unittest.mock import AsyncMock, Mock

import discord
import pytest

from core.message_handler import CommandHandler, Command


class TestCommandHandlerInit:
    def test_failed_prefix_with_spaces(self):
        with pytest.raises(ValueError):
            CommandHandler('p pa')

    def test_failed_empty_prefix(self):
        with pytest.raises(ValueError):
            CommandHandler('')


class TestIsCommand:
    @pytest.mark.parametrize('value,prefix',
                             [('!start with data', '!'), ('$привет всем', '$'), ('/help', '/')])
    def test_is_command_matched(self, value, prefix):
        assert CommandHandler(prefix).is_command(value) is True

    @pytest.mark.parametrize('value,prefix', [('start', '!'), ('$привет', '$$'), ('/help', '!')])
    def test_is_command_miss(self, value, prefix):
        assert CommandHandler(prefix).is_command(value) is False


class TestExtractCommand:
    data = [
        ('!start', '!', Command('start', '')),
        ('$привет как дела', '$', Command('привет', 'как дела')),
        ('/help someone with this young man', '/', Command('help', 'someone with this young man'))
    ]

    @pytest.mark.parametrize('value,prefix,expected', data)
    def test_extract_command(self, value, prefix, expected):
        assert CommandHandler(prefix).extract_command(value) == expected


def test_handler_register():
    ch = CommandHandler()

    @ch.command('start')
    async def some_command_handler(*args, **kwargs):
        pass

    assert len(ch._bot_handlers) > 0, 'handler registration failed'


@pytest.mark.asyncio
async def test_handler_match():
    mock_message = Mock()
    ch = CommandHandler()

    handler_mock = AsyncMock()
    ch.command('start')(handler_mock)

    mock_message.content = 'usual message'
    await ch.call(mock_message)
    handler_mock.assert_not_called()

    mock_message.content = '!start'
    await ch.call(mock_message)
    handler_mock.assert_called_once()
